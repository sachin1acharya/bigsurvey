import { Component, OnInit, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Platform, NavController } from '@ionic/angular';
import { Crop } from '@ionic-native/crop/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Router } from '@angular/router';

@Component({
  selector: 'app-take-picture',
  templateUrl: './take-picture.page.html',
  styleUrls: ['./take-picture.page.scss'],
  encapsulation: ViewEncapsulation.None

})
export class TakePicturePage implements OnInit {

  currentImage;
  isSubmit: any = false;
  public options: any;
  constructor(private androidPermissions: AndroidPermissions,
    private webview: WebView,
    private filePath: FilePath,
    private camera: Camera, public platform: Platform,
    private crop: Crop, public navCtrl: NavController,
    public router: Router) {

    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
      result => console.log('Has permission?', result.hasPermission),
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA)
    );

 


  }

  ngOnInit() {
  }



  startTask() {
    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.GET_ACCOUNTS]).then(response => {
      console.log(response);
      this.isSubmit = true;
      var options: CameraOptions = {
        quality: 100,
        sourceType: this.camera.PictureSourceType.CAMERA,
        saveToPhotoAlbum: false,
        correctOrientation: true
      };

      this.camera.getPicture(options).then(imagePath => {

        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {

            this.crop.crop(imagePath, { quality: 75 })
              .then(
                newImage => {
                  this.currentImage = this.webview.convertFileSrc(filePath);
                },
                error => console.error('Error cropping image', error)
              );

          });

      });
    })


  }




  navigate() {
    this.router.navigate(['thankyou'])
  }
}
