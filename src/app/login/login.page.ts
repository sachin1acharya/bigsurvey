import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginPage implements OnInit {
  private componentModel;
  public isButtonClicked;
  constructor(public router: Router, public navCtrl: NavController) {

    this.componentModel = {
      username: "",
      password: ""
    };




  }
  ngOnInit() {
  }

  onSubmitLoginForm() {
    this.navCtrl.navigateRoot('/verify-otp');
  }


}
