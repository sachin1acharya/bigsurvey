import { Component, OnInit, ViewEncapsulation, ElementRef, ViewChild } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-survey',
  templateUrl: './survey.page.html',
  styleUrls: ['./survey.page.scss'],
  encapsulation: ViewEncapsulation.None

})
export class SurveyPage implements OnInit {
  message:any=null;
  public surveyQuestions = {
    label: ' How do you greet your customerwhen he enters and when he asks for a diet coke and you do not have a stock for that?',
    data:
      [{
        id: "0",
        label: 'Hi,How are you'
      },
      {
        id: "1",
        label: 'Hey, are you dieting'
      },
      {
        id: "2",
        label: 'We do not have it.'
      },
      {
        id: "3",
        label: 'We will get it delivered'
      },
      ]
  }

  public answer = {
    id: "3",
    label: 'We will get it delivered'
  }

  constructor(public navCtrl: NavController,public router:Router) {
    this.surveyQuestions.data.map(o => o['class'] = '');

  }

  ngOnInit() {
  }

  onItemClick(item, i) {
    
    this.surveyQuestions.data.map(o => o['class'] = '');
    if (item.label == this.answer.label) {
      this.surveyQuestions.data[i]['class'] = "correctAnswer";
      this.message=1;
    }
    else {
      this.surveyQuestions.data[i]['class'] = " wrongAnswer";
      this.message=2;

    }


  }


  navigate(){
    this.router.navigate(['take-picture']);
    
  }
}
