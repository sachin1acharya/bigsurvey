import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-verify-otp',
  templateUrl: './verify-otp.page.html',
  styleUrls: ['./verify-otp.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class VerifyOtpPage implements OnInit {

  timer;
  maxtime: any = 30;
  hidevalue: any = true;
  otp: any = "";
  isVerify: boolean = false;
  constructor(public router: Router, public navCtrl: NavController) {


  }
  ngOnInit() {
  }

  onSubmitLoginForm() {

    if (this.isVerify) {
      this.navCtrl.navigateRoot('/survey');
    }
    else {
      this.isVerify = true;
      this.startTimer();
    }


  }

  startTimer() {
    this.timer = setTimeout(x => {
      if (this.maxtime <= 0) {
      }
      this.maxtime -= 1;
      if (this.maxtime > 0) {
        this.hidevalue = false;
        this.startTimer();
      }

      else {
        this.hidevalue = true;
        this.otp = "1000";
      }

    }, 1000);


  }


  resendOTP() {
    this.otp = "";
    this.maxtime=30;
    this.startTimer();
  }


  otpController(event, next, prev) {
    if (event.target.value.length < 1 && prev) {
      prev.setFocus()
    }
    else if (next && event.target.value.length > 0) {
      next.setFocus();
    }
    else {
      return 0;
    }
  }

}
